# COIN Mobile Connect PHP SDK Samples

For extensive documentation on the PHP Mobile Connect SDK, please have a look at the following [README](../mobile-connect-sdk/README.md).

### Usage
Clone this Git project and enter your configurations in `bootstrap.php` in this directory. Run `composer install` and `composer run-script mc-samples`
in the root directory to execute the tests.
