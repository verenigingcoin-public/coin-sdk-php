<?php

use coin\sdk\mc\v3\domain\DiscoveryRequest;
use coin\sdk\mc\v3\service\impl\MobileConnectClientErrorHandler;
use coin\sdk\mc\v3\service\impl\MobileConnectService;
use PHPUnit\Framework\TestCase;

class DiscoverySample extends TestCase
{
    private MobileConnectService $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = new MobileConnectService(new MobileConnectClientErrorHandler());
    }

    public function testSendDiscoveryRequest(): void
    {
        $response = $this->service->sendDiscoveryRequest(new DiscoveryRequest("31610101010"));
        echo "\n$response\n";
    }
}
