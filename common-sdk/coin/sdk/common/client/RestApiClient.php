<?php

namespace coin\sdk\common\client;

use coin\sdk\common\crypto\ApiClientUtil;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class RestApiClient
{
    public static function getFullVersion() {
        // NOTE: automatically updated by pre_tag_command
        return 'coin-sdk-php-2.4.0';
    }

    protected $hmacSecret;
    protected $privateKey;
    protected $consumerName;
    protected $validPeriodInSeconds;

    function __construct($consumerName = null, $privateKeyFile = null, $encryptedHmacSecretFile = null, $validPeriodInSeconds = 30)
    {
        $this->privateKey = ApiClientUtil::readPrivateKeyFile($privateKeyFile ?: @$_ENV['PRIVATE_KEY_FILE'] ?: $GLOBALS['PrivateKeyFile']);
        $this->hmacSecret = ApiClientUtil::hmacSecretFromEncryptedFile($encryptedHmacSecretFile ?: @$_ENV['ENCRYPTED_HMAC_SECRET_FILE'] ?: $GLOBALS['EncryptedHmacSecretFile'], $this->privateKey);
        $this->consumerName = $consumerName ?: @$_ENV['CONSUMER_NAME'] ?: $GLOBALS['ConsumerName'];
        $this->validPeriodInSeconds = $validPeriodInSeconds;
    }

    /**
     * @param int $method
     * @param string $url
     * @param string $content [optional]
     * @param array $options Request options to apply. See \GuzzleHttp\RequestOptions.
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function sendWithToken($method, $url, $content = null, $options = [])
    {
        $client = new Client();
        $hmacHeaders = ApiClientUtil::getHmacHeaders($content);
        $methodName = $method;
        $params = parse_url($url, PHP_URL_QUERY);
        $query = $params ? '?' . $params : '';
        $localPath = parse_url($url, PHP_URL_PATH);
        $requestLine = "$methodName $localPath$query HTTP/1.1";
        $hmac = ApiClientUtil::CalculateHttpRequestHmac($this->hmacSecret, $this->consumerName, $hmacHeaders, $requestLine);
        $jwt = ApiClientUtil::createJwt($this->privateKey, $this->consumerName, $this->validPeriodInSeconds);
        $options['body'] = $content;
        $options['headers'] = array_merge($options['headers'] ?? [], $hmacHeaders, array(
                "Authorization" => $hmac,
                "User-Agent" => $this::getFullVersion(),
                'Content-Type' => 'application/json; charset=utf-8',
                "cookie" => "jwt=$jwt;")
        );
        return $client->request($method, $url, $options);
    }
}
