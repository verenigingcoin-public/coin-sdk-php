<?php

namespace coin\sdk\mc\v3\service;

use coin\sdk\mc\v3\domain\DiscoveryRequest;
use coin\sdk\mc\v3\domain\DiscoveryResponse;
use coin\sdk\mc\v3\domain\ErrorResponse;
use coin\sdk\mc\v3\service\impl\MobileConnectClientErrorHandler;
use coin\sdk\mc\v3\service\impl\MobileConnectService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DiscoveryTestCustomErrorHandlerTest extends TestCase
{
    private MobileConnectService $service;
    private MockObject $errorHandler;

    protected function setUp(): void
    {
        $this->errorHandler = $this->createMock(MobileConnectClientErrorHandler::class);
        $this->service = new MobileConnectService($this->errorHandler);
    }

    public function testSendDiscoveryRequest(): void
    {
        $this->errorHandler->expects($this->never())->method('onNotFound');
        $this->errorHandler->expects($this->never())->method('onOtherError');
        $response = $this->service->sendDiscoveryRequest(new DiscoveryRequest("123456789"));
        $this->assertInstanceOf(DiscoveryResponse::class, $response);
    }

    public function testSendDiscoveryRequestNotFound(): void
    {
        $request = new DiscoveryRequest("123456789", "404");
        $this->errorHandler->expects($this->once())->method('onNotFound')->with($request, self::isInstanceOf(ErrorResponse::class));
        $this->errorHandler->expects($this->never())->method('onOtherError');
        $this->assertNull($this->service->sendDiscoveryRequest($request));
    }

    public function testSendDiscoveryRequestOtherError(): void
    {
        $request = new DiscoveryRequest("123456789", "403");
        $this->errorHandler->expects($this->never())->method('onNotFound');
        $this->errorHandler->expects($this->once())->method('onOtherError')->with($request, self::anything());
        $this->assertNull($this->service->sendDiscoveryRequest($request));
    }
}
