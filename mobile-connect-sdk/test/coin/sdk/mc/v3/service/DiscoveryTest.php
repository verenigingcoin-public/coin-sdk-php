<?php

namespace coin\sdk\mc\v3\service;

use coin\sdk\mc\v3\domain\DiscoveryRequest;
use coin\sdk\mc\v3\domain\DiscoveryResponse;
use coin\sdk\mc\v3\domain\UnexpectedDiscoveryException;
use coin\sdk\mc\v3\service\impl\MobileConnectClientErrorHandler;
use coin\sdk\mc\v3\service\impl\MobileConnectService;
use PHPUnit\Framework\TestCase;

class DiscoveryTest extends TestCase
{
    private MobileConnectService $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = new MobileConnectService(new MobileConnectClientErrorHandler());
    }

    public function testSendDiscoveryRequest(): void
    {
        $response = $this->service->sendDiscoveryRequest(new DiscoveryRequest("123456789"));
        $this->assertInstanceOf(DiscoveryResponse::class, $response);
    }

    public function testSendDiscoveryRequestNotFound(): void
    {
        $this->assertNull($this->service->sendDiscoveryRequest(new DiscoveryRequest("123456789", "404")));
    }

    public function testSendDiscoveryRequestOtherError(): void
    {
        $this->expectException(UnexpectedDiscoveryException::class);
        $this->service->sendDiscoveryRequest(new DiscoveryRequest("123456789", "403"));
    }
}

