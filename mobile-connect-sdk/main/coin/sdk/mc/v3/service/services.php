<?php

namespace coin\sdk\mc\v3\service\impl;

use coin\sdk\mc\v3\domain\DiscoveryRequest;
use coin\sdk\mc\v3\domain\ErrorResponse;
use coin\sdk\mc\v3\domain\UnexpectedDiscoveryException;
use Psr\Http\Message\ResponseInterface;

interface IMobileConnectService {
    public function sendDiscoveryRequest(DiscoveryRequest $request);
}

interface IMobileConnectClientErrorHandler {
    public function onNotFound(DiscoveryRequest $request, ErrorResponse $errorResponse);

    /**
     * @throws UnexpectedDiscoveryException
     */
    public function onOtherError(DiscoveryRequest $request, ResponseInterface $response);
}
