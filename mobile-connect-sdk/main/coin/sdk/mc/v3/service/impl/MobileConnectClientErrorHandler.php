<?php

namespace coin\sdk\mc\v3\service\impl;

use coin\sdk\mc\v3\domain\DiscoveryRequest;
use coin\sdk\mc\v3\domain\ErrorResponse;
use coin\sdk\mc\v3\domain\UnexpectedDiscoveryException;
use Psr\Http\Message\ResponseInterface;

class MobileConnectClientErrorHandler implements IMobileConnectClientErrorHandler
{

    public function onNotFound(DiscoveryRequest $request, ErrorResponse $errorResponse): void
    {
    }

    public function onOtherError(DiscoveryRequest $request, ResponseInterface $response): void
    {
        throw new UnexpectedDiscoveryException($request, $response);
    }
}
