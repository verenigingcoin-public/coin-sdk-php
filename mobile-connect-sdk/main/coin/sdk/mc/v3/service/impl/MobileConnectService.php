<?php

namespace coin\sdk\mc\v3\service\impl;

use coin\sdk\common\client\RestApiClient;
use coin\sdk\mc\v3\domain\DiscoveryRequest;
use coin\sdk\mc\v3\domain\DiscoveryResponse;
use coin\sdk\mc\v3\domain\ErrorResponse;
use coin\sdk\mc\v3\domain\UnexpectedDiscoveryException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class MobileConnectService extends RestApiClient implements IMobileConnectService
{
    private string $discoveryUrl;
    private IMobileConnectClientErrorHandler $errorHandler;

    public function __construct(IMobileConnectClientErrorHandler $errorHandler, $consumerName = null, $privateKeyFile = null, $encryptedHmacSecretFile = null, $coinBaseUrl = null) {
        parent::__construct(
            $consumerName,
            $privateKeyFile,
            $encryptedHmacSecretFile
        );
        $this->discoveryUrl = ($coinBaseUrl ?: @$_ENV['COIN_BASE_URL'] ?: $GLOBALS['CoinBaseUrl']).'/mobile-connect/v3/discovery';
        $this->errorHandler = $errorHandler;
    }

    /**
     * @throws GuzzleException
     * @throws UnexpectedDiscoveryException
     */
    public function sendDiscoveryRequest(DiscoveryRequest $request): ?DiscoveryResponse
    {
        $response = $this->sendWithToken('POST', $this->discoveryUrl, json_encode($request), [RequestOptions::HTTP_ERRORS => false]);
        $statusCode = $response->getStatusCode();
        $responseBody = json_decode($response->getBody()->getContents());
        if ($statusCode < 300 && $statusCode >= 200) {
            return DiscoveryResponse::fromObject($responseBody);
        } else if ($statusCode == 404 && $responseBody->error && $responseBody->description) {
            $this->errorHandler->onNotFound($request, ErrorResponse::fromObject($responseBody));
            return null;
        }
        $this->errorHandler->onOtherError($request, $response);
        return null;
    }
}
