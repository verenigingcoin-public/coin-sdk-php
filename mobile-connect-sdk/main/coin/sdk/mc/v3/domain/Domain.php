<?php

namespace coin\sdk\mc\v3\domain;

use Exception;
use Psr\Http\Message\ResponseInterface;

class DiscoveryRequest {
    public string $msisdn;
    public ?string $correlationId;

    function __construct(string $msisdn, ?string $correlationId = null) {
        $this->msisdn = $msisdn;
        $this->correlationId = $correlationId;
    }

    public function __toString(): string
    {
        return "DiscoveryRequest(msisdn = $this->msisdn, correlationId = $this->correlationId)";
    }
}

class DiscoveryResponse {
    public string $networkOperatorCode;
    public SupportedServices $supportedServices;
    public ?string $correlationId;
    public ?string $clientId;
    public ?string $clientSecret;

    static function fromObject(object $obj): DiscoveryResponse
    {
        $response = new DiscoveryResponse();
        $response->networkOperatorCode = $obj->networkOperatorCode;
        $response->supportedServices = SupportedServices::fromObject($obj->supportedServices);
        $response->correlationId = $obj->correlationId;
        $response->clientId = $obj->clientId;
        $response->clientSecret = $obj->clientSecret;
        return $response;
    }

    public function __toString(): string
    {
        return "DiscoveryResponse(networkOperatorCode = $this->networkOperatorCode, supportedServices = $this->supportedServices, correlationId = $this->correlationId, clientId = $this->clientId, clientSecret = $this->clientSecret)";
    }
}

class SupportedServices {
    public array $match;
    public array $numberVerify;
    public array $accountTakeoverProtection;

    static function fromObject(object $obj): SupportedServices
    {
        $supportedServices = new SupportedServices();
        $callback = fn(object $m): Link => Link::fromObject($m);
        $supportedServices->match = array_map($callback, $obj->match);
        $supportedServices->numberVerify = array_map($callback, $obj->numberVerify);
        $supportedServices->accountTakeoverProtection = array_map($callback, $obj->accountTakeoverProtection);
        return $supportedServices;
    }

    public function __toString(): string
    {
        $match = implode(", ", $this->match);
        $numberVerify = implode(", ", $this->numberVerify);
        $accountTakeoverProtection = implode(", ", $this->accountTakeoverProtection);
        return "SupportedServices(match = [$match], numberVerify = [$numberVerify], accountTakeoverProtection = [$accountTakeoverProtection])";
    }
}

class Link {
    public string $rel;
    public string $href;

    static function fromObject(object $obj): Link
    {
        $link = new Link();
        $link->rel = $obj->rel;
        $link->href = $obj->href;
        return $link;
    }

    public function __toString(): string
    {
        return "Link(rel = $this->rel, href = $this->href)";
    }
}

class ErrorResponse {
    public string $error;
    public string $description;
    public ?string $correlationId;

    static function fromObject(object $obj): ErrorResponse
    {
        $errorResponse = new ErrorResponse();
        $errorResponse->error = $obj->error;
        $errorResponse->description = $obj->description;
        $errorResponse->correlationId = $obj->correlation_id;
        return $errorResponse;
    }
}

class UnexpectedDiscoveryException extends Exception {
    public DiscoveryRequest $request;
    public ResponseInterface $response;

    function __construct(DiscoveryRequest $request, ResponseInterface $response) {
        $this->request = $request;
        $this->response = $response;
        parent::__construct();
    }
}
