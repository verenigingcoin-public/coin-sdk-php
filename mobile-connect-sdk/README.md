# PHP Mobile Connect SDK

## Introduction

This SDK supports secured access to the Mobile Connect API.


## Setup

#### Usage
Run `composer require coin/sdk` in your own project to use this SDK.

#### Sample Project for the Mobile Connect API
A sample project is provided in the `mobile-connect-sdk-samples` directory.


## Configure Credentials

For secure access credentials are required.
- Check [this README](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md) to find out how to configure these.
- In summary, you will need:
    - a consumer name
    - a private key file
    - a file containing the encrypted Hmac secret
    - the base url of the Coin platform you are using
- These can be used to create instances of the `MobileConnectService`.
If you instantiate this without consumer name etc., the required values will be searched in `$_ENV` and subsequently in `$GLOBALS`
under the following names:

| $_ENV                      | $GLOBALS                |
|----------------------------|-------------------------|
| CONSUMER_NAME              | ConsumerName            |
| PRIVATE_KEY_FILE           | PrivateKeyFile          |
| ENCRYPTED_HMAC_SECRET_FILE | EncryptedHmacSecretFile |
| COIN_BASE_URL              | CoinBaseUrl             |

For populating the `$_ENV` variable, [PHP dotenv](https://github.com/vlucas/phpdotenv) could be used.


## Code
The `MobileConnectService->sendDiscoveryRequest()` method can be used to send discovery requests.
To create this service, the aforementioned credentials are needed.
In addition, its constructor requires an `IMobileConnectClientErrorHandler`.
For this, you can create a custom implementation or use the provided `MobileConnectClientErrorHandler`.
This handler ignores `404 not found` errors from the api, which results in the `sendDiscoveryRequest()` method
returning `null`. Other errors result in an exception being thrown.
